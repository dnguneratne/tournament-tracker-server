import passport from 'passport';
import passportJWT from 'passport-jwt';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import config from '../config/config';
import logger from './winston';
import Admins from '../database/models/admin.model';
import Players from '../database/models/player.model';

const extractJwt = passportJWT.ExtractJwt;
const JwtStrategy = passportJWT.Strategy;

const jwtOptions = {};

jwtOptions.jwtFromRequest = extractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = config.jwtSecret;

const strategy = new JwtStrategy(jwtOptions, (jwtPayload, next) => {
  logger.log('info', 'Secure endpoint reached. Processing Passport stratergy');

  if (jwtPayload.id) {
    logger.log('debug', `payload recieved ${jwtPayload.id}`);

    return Admins.findById(jwtPayload.id, (err, user) => {
      if (err) {
        logger.log('error', 'Error looking up user in db');
        return next(new APIError('Internal Server Error', httpStatus.UNAUTHORIZED, true));
      }
      if (user) {
        logger.log('info', `User ${user.name.first_name} ${user.name.last_name} with role ${user.role} granted access to endpoint`);
        return next(null, user);
      }

      // If user is not in the Admins collection are they in the Players collection?
      return Players.findById(jwtPayload.id, (error, player) => {
        if (error) {
          logger.log('error', 'Error looking up user in db');
          return next(new APIError('Internal Server Error', httpStatus.UNAUTHORIZED, true));
        }
        if (player) {
          logger.log('info', `User ${player.name.first_name} ${player.name.last_name} with role ${player.role} granted access to endpoint`);
          return next(null, player);
        }
        logger.log('info', 'Could not find user in db');
        return next(new APIError('Internal Server Error', httpStatus.UNAUTHORIZED, true));
      });
    });
  }
  logger.log('error', 'Invalid token. No id present in token');
  return next(new APIError('Internal Server Error', httpStatus.UNAUTHORIZED, true));
});

passport.use(strategy);

export default passport;
