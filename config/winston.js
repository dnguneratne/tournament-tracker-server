import winston from 'winston';

const fs = require('fs');

const env = process.env.NODE_ENV || 'development';
const logDir = 'logs';
const tsFormat = () => (new Date()).toLocaleTimeString();

// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
  fs.mkdirSync(logDir);
}

const logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      timestamp: tsFormat,
      colorize: true,
      json: true,
      level: 'silly',
    }),
    new (winston.transports.File)({
      name: 'info-file',
      filename: `${logDir}/info.log`,
      timestamp: tsFormat,
      level: env === 'development' ? 'debug' : 'info',
      json: true,
    }),
  ],
  exceptionHandlers: [
    new winston.transports.File({
      filename: `${logDir}/exceptions.log`,
      timestamp: tsFormat,
      json: true,
    }),
  ],
  exitOnError: false,
});

export default logger;
