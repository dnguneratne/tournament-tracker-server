import mongoose from 'mongoose';
import util from 'util';
import config from './config';
import logger from './winston';

// Connect to mongodb
const mongoUri = config.mongo.host;
mongoose.connect(`mongodb://${mongoUri}`, { useMongoClient: true });

// Get Mongoose to use the global promise library
mongoose.Promise = global.Promise;

mongoose.connection.on('error', () => {
  logger.log('crit', `Unable to connect to database: ${mongoUri}`);
  throw new Error(`Unable to connect to database: ${mongoUri}`);
});

mongoose.connection.on('open', () => logger.log('info', 'Connected to MongoDB'));

// print mongoose logs in dev env
if (config.mongooseDebug) {
  mongoose.set('debug', (collectionName, method, query, doc) => {
    logger.log('debug', `${collectionName}.${method}`, util.inspect(query, false, 20), doc);
  });
}

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', () => {
  mongoose.connection.close(() => {
    logger.log('info', 'Mongoose default connection disconnected through app termination');
    process.exit(0);
  });
});
