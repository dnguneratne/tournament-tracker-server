import express from 'express';
import logger from 'morgan';
import path from 'path';
import httpStatus from 'http-status';
import bodyParser from 'body-parser';
import cors from 'cors';
import helmet from 'helmet';
import approotdir from 'app-root-dir';
import expressValidation from 'express-validation';
import winston from 'winston';
import expressWinston from 'express-winston';
import passport from './passport';
import config from './config';
import routes from '../routes/index.route';
import APIError from '../helpers/APIError';

const appRootDir = approotdir.get();

const app = express();
app.use(passport.initialize({ userProperty: 'authenticatedUser' })); // Passport by default next(user) is called by req.user. This will change it to req.authenticatedUser
app.use(bodyParser.json()); // parse body params and attache them to req.body
app.use(bodyParser.urlencoded({ extended: true }));
app.use(helmet()); // secure apps by setting various HTTP headers
app.use(cors()); // enable CORS - Cross Origin Resource Sharing
if (config.env === 'development') {
  app.use(logger('dev'));
}

// enable detailed API logging in dev env
const logDir = 'logs';
const tsFormat = () => (new Date()).toLocaleTimeString();
if (config.env === 'development') {
  expressWinston.requestWhitelist.push('body');
  expressWinston.responseWhitelist.push('body');
  app.use(expressWinston.logger({
    transports: [
      new (winston.transports.Console)({
        timestamp: tsFormat,
        colorize: true,
      }),
      new (winston.transports.File)({
        name: 'http-file',
        filename: `${logDir}/http.log`,
        timestamp: tsFormat,
        json: true,
      }),
    ],
    meta: true, // optional: log meta data about request (defaults to true)
    msg: 'HTTP {{req.method}} {{req.url}} {{res.statusCode}} {{res.responseTime}}ms',
    colorStatus: true, // Color the status code (default green, 3XX cyan, 4XX yellow, 5XX red).
  }));
}

// mount all routes on /api path
app.use('/api', routes);

// All calls from Angular will use Angular dist folder
app.use('/', express.static(path.join(appRootDir, '../tournament-tracker-client/dist')));
app.use('*', express.static(path.join(appRootDir, '../tournament-tracker-client/dist')));

// if error is not an instanceOf APIError, convert it.
app.use((err, req, res, next) => {
  if (err instanceof expressValidation.ValidationError) {
    // validation error contains errors which is an array of error each containing message[]
    const unifiedErrorMessage = err.errors.map(error => error.messages.join('. ')).join(' and ');
    const error = new APIError(unifiedErrorMessage, err.status, true);
    return next(error);
  } else if (!(err instanceof APIError)) {
    const apiError = new APIError(err.message, err.status, err.isPublic);
    return next(apiError);
  }
  return next(err);
});

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new APIError('API not found', httpStatus.NOT_FOUND);
  return next(err);
});

// log error in winston transports except when executing test suite
if (config.env !== 'test') {
  app.use(expressWinston.errorLogger({
    transports: [
      new (winston.transports.File)({
        name: 'error-file',
        filename: `${logDir}/http-errors.log`,
        level: 'error',
        timestamp: tsFormat,
        json: true,
      }),
    ],
  }));
}

// error handler, send stacktrace only during development
app.use((err, req, res, next) => { // eslint-disable-line no-unused-vars
  res.status(err.status).json({
    message: err.isPublic ? err.message : httpStatus[err.status],
    stack: config.env === 'development' ? err.stack : {},
  });
});

export default app;
