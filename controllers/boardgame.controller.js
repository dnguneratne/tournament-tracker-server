import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import Boardgame from '../database/models/boardgame.model';
import logger from '../config/winston';

async function create(req, res, next) {
  logger.log('info', `Request to add board game ${req.body.name}`);

  try {
    const gameName = req.body.name.toLowerCase();
    const scoreRank = req.body.scoring;
    const maxPlayers = req.body.max_num_players;
    const minPlayers = req.body.min_num_players;

    // find by game name (Adds new document if game names are different. Cannot update game name)
    const game = await Boardgame.findOneAndUpdate(
      { name: gameName },
      {
        name: gameName,
        scoring: scoreRank,
        max_num_players: maxPlayers,
        min_num_players: minPlayers,
      }, { new: true, upsert: true },
    );

    if (!game) {
      logger.log('error', 'Could not add new board game');
      return next(new APIError('Internal Server Error', httpStatus.INTERNAL_SERVER_ERROR, true));
    }

    logger.log('info', `Board game ${game.name} added`);
    return res.json({
      success: true,
      new_game: game,
    });
  } catch (err) {
    logger.log('error', 'DB error on adding new board game');
    logger.log('error', err);
    return next(new APIError('Internal Server Error', httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

async function getAll(req, res, next) {
  logger.log('info', 'Request to get all board games');

  try {
    const games = await Boardgame.find();

    if (!games) {
      logger.log('error', 'Error finding games. No games in db.');
      return next(new APIError('Internal Server Error', httpStatus.INTERNAL_SERVER_ERROR, true));
    }

    if (!games.length) { // No games in the db
      logger.log('info', 'Could not find any games');
      return res.json({
        success: false,
        message: 'Could not find any games',
      });
    }

    logger.log('info', 'Get all board games success');
    return res.json({
      success: true,
      games,
    });
  } catch (err) {
    logger.log('error', 'DB error on get all board games');
    logger.log('error', err);
    return next(new APIError('Internal Server Error', httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}


async function load(req, res, next, gameId) {
  logger.log('info', `Incoming request containing param gameId: ${gameId}. Loading single game`);

  try {
    const game = await Boardgame.findById(gameId);

    if (!game) {
      logger.log('info', `Board game with Id: ${gameId} not found.`);
      return next(new APIError('Internal Server Error', httpStatus.UNPROCESSABLE_ENTITY, true));
    }

    logger.log('info', `Loaded single board game ${game.name}`);
    req.game = game;
    return next();
  } catch (err) {
    logger.log('error', 'DB error on finding single board game');
    logger.log('error', err);
    return next(new APIError('Internal Server Error', httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

function getOne(req, res) {
  logger.log('info', 'Requesting single game by ID');
  logger.log('info', `Responding with single game ${req.game.name}`);
  return res.json(req.game);
}

async function update(req, res, next) {
  logger.log('info', `Requesting update of game ${req.game.name}`);

  try {
    const gamename = req.body.name.toLowerCase();
    const scoreRank = req.body.scoring;
    const maxPlayers = req.body.max_num_players;
    const minPlayers = req.body.min_num_players;

    const updatedGame = await Boardgame.findByIdAndUpdate(req.game.id, {
      $set: {
        name: gamename,
        scoring: scoreRank,
        max_num_players: maxPlayers,
        min_num_players: minPlayers,
      },
    }, { new: true });

    if (!updatedGame) {
      logger.log('info', `Failed to updated Board game : ${gamename} not found.`);
      return next(new APIError('Internal Server Error', httpStatus.UNPROCESSABLE_ENTITY, true));
    }

    logger.log('info', `Board game ${updatedGame.name} successfully updated`);
    return res.json(updatedGame.name);
  } catch (err) {
    logger.log('error', 'DB error on finding single board game');
    logger.log('error', err);
    return next(new APIError('Internal Server Error', httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

export default {
  create, load, getAll, getOne, update,
};
