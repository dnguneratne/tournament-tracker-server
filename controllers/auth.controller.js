import jwt from 'jsonwebtoken';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import config from '../config/config';
import Admins from '../database/models/admin.model';
import logger from '../config/winston';

function login(req, res, next) {
  const usernm = req.body.username.toLowerCase();
  const pass = req.body.password.toLowerCase();
  logger.log('info', `User ${usernm} requesting authentication`);

  Admins.findOne({
    username: usernm,
    password: pass,
  }, (err, usr) => {
    if (err) {
      logger.log('error', 'db error on admin login');
      logger.log('error', err);
      return next(new APIError('Internal Server Error', httpStatus.INTERNAL_SERVER_ERROR, true));
    }
    if (!usr) {
      logger.log('error', `User ${usernm} not found`);
      return next(new APIError('User not found', httpStatus.UNAUTHORIZED, true));
    }

    logger.log('info', `User ${usr.username} with ${usr.id} found. Token sent.`);
    const token = jwt.sign({
      id: usr.id,
    }, config.jwtSecret, { expiresIn: '1h' });

    return res.json({
      success: true,
      id: usr.id,
      token: `bearer ${token}`,
    });
  });
}

function load(req, res, next, adminId) {
  logger.log('info', `Incoming request containing param Admin id: ${adminId}. Loading single admin`);

  Admins.findById(adminId, (err, admin) => {
    if (err) {
      logger.log('error', 'DB error on finding single admin');
      logger.log('error', err);
      return next(new APIError('Internal Server Error', httpStatus.INTERNAL_SERVER_ERROR, true));
    }
    if (!admin) {
      logger.log('info', `Admin with Id: ${adminId} not found.`);
      return next(new APIError('Internal Server Error', httpStatus.UNPROCESSABLE_ENTITY, true));
    }
    logger.log('info', `Loaded single Admin: ${admin.name}`);
    req.user = admin;
    return next();
  });
}

export default { login, load };
