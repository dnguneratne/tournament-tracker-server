import jwt from 'jsonwebtoken';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import config from '../config/config';
import Player from '../database/models/player.model';
import logger from '../config/winston';

async function create(req, res, next) {
  try {
    const fname = req.body.name.first_name.toLowerCase();
    const lname = req.body.name.last_name.toLowerCase();
    logger.log('info', `Player ${fname} ${lname} requesting registration`);

    const player = await Player.findOne({
      name: {
        last_name: lname,
        first_name: fname,
      },
    });

    if (player) {
      logger.log('info', `Player ${fname} ${lname} already in db.`);
      return next(new APIError('Internal Server Error', httpStatus.UNPROCESSABLE_ENTITY, true));
    }

    logger.log('info', `Player ${fname} ${lname} not found. Registering new player`);

    const newPlayer = new Player({
      name: {
        first_name: fname,
        last_name: lname,
      },
    });

    const savedPlayer = await newPlayer.save();

    if (!savedPlayer) {
      logger.log('error', 'Could not register new player');
      return next(new APIError('Internal Server Error', httpStatus.INTERNAL_SERVER_ERROR, true));
    }

    logger.log('info', `Player ${fname} ${lname} registered. Token sent.`);

    const token = jwt.sign({
      id: savedPlayer.id,
      name: {
        first_name: savedPlayer.name.first_name,
        last_name: savedPlayer.name.last_name,
      },
    }, config.jwtSecret, { expiresIn: '1h' });

    return res.json({
      id: savedPlayer.id,
      success: true,
      token: `bearer ${token}`,
      player: {
        first_name: savedPlayer.name.first_name,
        last_name: savedPlayer.name.last_name,
      },
    });
  } catch (err) {
    logger.log('error', 'Error on player registration');
    logger.log('error', err);
    return next(new APIError('Internal Server Error', httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

function load(req, res, next, playerId) {
  logger.log('info', `Incoming request containing param playerId: ${playerId}. Loading single player`);

  Player.findById(playerId, (err, player) => {
    if (err) {
      logger.log('error', 'DB error on finding single player');
      logger.log('error', err);
      return next(new APIError('Internal Server Error', httpStatus.INTERNAL_SERVER_ERROR, true));
    }
    if (!player) {
      logger.log('info', `Player with playerId: ${playerId} not found.`);
      return next(new APIError('Internal Server Error', httpStatus.UNPROCESSABLE_ENTITY, true));
    }
    logger.log('info', `Loaded single player ${player.name.first_name} ${player.name.last_name}`);
    req.user = player;
    return next();
  });
}

async function update(req, res, next) {
  logger.log('info', `Requesting update of player ${req.user.name.first_name} ${req.user.name.last_name}`);

  const fname = req.body.name.first_name;
  const lname = req.body.name.last_name;

  try {
    const updatedPlayer = await Player.findByIdAndUpdate(req.user.id, {
      $set: {
        name: {
          first_name: fname,
          last_name: lname,
        },
      },
    }, { new: true });

    if (!updatedPlayer) {
      logger.log('info', `Player ${fname} ${lname} not found.`);
      return next(new APIError('Internal Server Error', httpStatus.UNPROCESSABLE_ENTITY, true));
    }

    logger.log('debug', updatedPlayer.name);

    logger.log('info', `Updated single player ${updatedPlayer.name.first_name} ${updatedPlayer.name.last_name}`);
    return res.json(updatedPlayer.name);
  } catch (err) {
    logger.log('error', 'DB error on finding single player');
    logger.log('error', err);
    return next(new APIError('Internal Server Error', httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

async function remove(req, res, next) {
  logger.log('info', 'Incoming request to remove player');

  try {
    const removedPlayer = await Player.findByIdAndRemove(req.user.id);

    if (!removedPlayer) {
      logger.log('info', 'Could not find player in db');
      return next(new APIError('Internal Server Error', httpStatus.UNPROCESSABLE_ENTITY, true));
    }

    logger.log('info', `Successfully removed player ${removedPlayer.name.first_name} ${removedPlayer.name.last_name}`);
    return res.json(removedPlayer.name);
  } catch (err) {
    logger.log('error', 'DB error on removing a player');
    logger.log('error', err);
    return next(new APIError('Internal Server Error', httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

async function list(req, res, next) {
  try {
    const players = await Player.find();

    if (!players) {
      logger.log('info', 'Could not retrive all players from db');
      return next(new APIError('Internal Server Error', httpStatus.UNPROCESSABLE_ENTITY, true));
    }

    logger.log('info', 'Successfully retrieved all players');
    return res.json(players);
  } catch (err) {
    logger.log('error', 'DB error on retrieving all players');
    logger.log('error', err);
    return next(new APIError('Internal Server Error', httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

export default {
  create, load, update, remove, list,
};
