import httpStatus from 'http-status';
import mongoose from 'mongoose';
import APIError from '../helpers/APIError';
import Tournament from '../database/models/tournament.model';
import logger from '../config/winston';

// Also creates if not in db
async function create(req, res, next) {
  logger.log('info', 'Incoming request to add new tournament');

  try {
    let tournamentId = req.body.id;
    let dte = req.body.date;
    const stats = req.body.status;
    const mtchs = req.body.matches;

    const tournament = await Tournament.findById(tournamentId).exec();

    // If tournament is not present in db create one
    if (!tournament) {
      if (!tournamentId) { tournamentId = new mongoose.Types.ObjectId(); }
      if (!dte) { dte = Date.now(); }

      const newTournament = new Tournament({
        date: dte,
        status: stats,
        matches: mtchs,
      });

      const savedTournament = await newTournament.save();

      if (!savedTournament) {
        logger.log('error', 'Failed to create a tournament');
        return next(new APIError('Internal Server Error', httpStatus.UNPROCESSABLE_ENTITY, true));
      }

      logger.log('info', 'New tournament successfully created');
      return res.json(savedTournament);
    }

    logger.log('error', 'Tournament already in db. Failed to create a tournament');
    return next(new APIError('Internal Server Error', httpStatus.UNPROCESSABLE_ENTITY, true));
  } catch (err) {
    logger.log('error', 'Error on adding/updating new tournament');
    logger.log('error', err);
    return next(new APIError('Internal Server Error', httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

async function list(req, res, next) {
  logger.log('info', 'Incoming request to retrieve all tournaments');

  try {
    const tournaments = await Tournament.find()
      .populate('matches.games.board_game matches.games.players');

    if (!tournaments) {
      logger.log('info', 'Failed to retrieve all tournaments');
      return next(new APIError('Internal Server Error', httpStatus.UNPROCESSABLE_ENTITY, true));
    }

    logger.log('info', 'Successfully retrieved all tournaments');
    return res.json(tournaments);
  } catch (err) {
    logger.log('error', 'Error on retrieving all tournaments');
    logger.log('error', err);
    return next(new APIError('Internal Server Error', httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

async function load(req, res, next, tournamentId) {
  logger.log('info', `Incoming request containing param Tournament id: ${tournamentId}. Loading single Tournament`);

  try {
    const tournament = await Tournament.findById(tournamentId)
      .populate('matches.games.board_game matches.games.players');

    if (!tournament) {
      logger.log('info', `Tournament with Id: ${tournamentId} not found.`);
      return next(new APIError('Internal Server Error', httpStatus.UNPROCESSABLE_ENTITY, true));
    }
    logger.log('info', 'Loaded single Tournament');
    req.tournament = tournament;
    return next();
  } catch (err) {
    logger.log('error', 'DB error on finding single Tournament');
    logger.log('error', err);
    return next(new APIError('Internal Server Error', httpStatus.INTERNAL_SERVER_ERROR, true));
  }
}

function getOne(req, res) {
  logger.log('info', 'Requesting single tournament by ID');
  return res.json(req.tournament);
}

export default {
  create, list, load, getOne,
};
