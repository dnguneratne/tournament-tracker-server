import mongoose from 'mongoose';

const BoardgameSchema = new mongoose.Schema({
  name: {
    type: String,
    unique: true,
    required: true,
    lowercase: true,
  },
  scoring: {
    type: Object,
    required: true,
    rank: {
      first_place: Number,
      second_place: Number,
      third_place: Number,
    },
  },
  max_num_players: Number,
  min_num_players: Number,
}, { runSettersOnQuery: true });

module.exports = mongoose.model('Boardgame', BoardgameSchema);
