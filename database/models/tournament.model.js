import mongoose from 'mongoose';

const TournamentSchema = new mongoose.Schema({
  date: {
    required: true,
    type: Date,
    default: Date.now,
  },
  status: {
    required: true,
    lowercase: true,
    type: String,
    enum: ['undefined', 'scheduled', 'enrolling', 'in progress', 'completed'],
    default: 'undefined',
  },
  matches: [{
    games: [{
      board_game: {
        type: mongoose.Schema.Types.Mixed,
        ref: 'Boardgame',
      },
      players: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Player',
      }],
    }],
  }],
});

module.exports = mongoose.model('Tournament', TournamentSchema);

// matches: [{
//   board_games: [{
//     type: mongoose.Schema.Types.Mixed,
//     ref: 'Boardgame',
//     players: [{
//       type: mongoose.Schema.Types.ObjectId,
//       ref: 'Player',
//     }],
//   }],
// }],

// {
//   "status": "enrolling",
//   "matches": [
//   	{ "board_games": [
//   		{
// 	  		"_id": "5a498918ffc6220edbe8a403",
// 	  		"players": [
// 	  			{ "_id": "5a4fa908d9d55465ac4fdbe6" },
// 	  			{ "_id": "5a50cf3d09176c2bb0f98fe1" }
// 	  		]
//   		},
//   		{
// 	  		"_id": "5a50cf9007c2bb0c73f3783a",
// 	  		"players": [
// 	  			{ "_id": "5a50cf5609176c2bb0f98fe2" },
// 	  			{ "_id": "5a50cf6009176c2bb0f98fe3" }
// 	  		]
//   		}
//   	]}
//   ]
// }

// {
//   "status": "enrolling",
//   "matches": [
//   	{ "board_games": [
//   		{
// 	  		"_id": "5a498918ffc6220edbe8a403"
//   		},
//   		{
// 	  		"_id": "5a50cf9007c2bb0c73f3783a"
//   		}
//   	]}
//   ]
// }
