import mongoose from 'mongoose';

const AdminSchema = new mongoose.Schema({
  name: {
    type: Object,
    required: true,
    lowercase: true,
    first_name: {
      type: String,
      lowercase: true,
      required: true,
    },
    last_name: {
      type: String,
      lowercase: true,
      required: true,
    },
  },
  username: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
  },
  password: {
    type: String,
    required: true,
    lowercase: true,
    unique: true,
  },
  role: {
    type: String,
    lowercase: true,
    enum: ['admin', 'client'],
    default: 'client',
  },
});

// Password encryption
// AdminSchema.pre('save', (next) => {
//   const user = this;
//   if (this.isModified('password') || this.isNew) {
//     bcrypt.genSalt
//   }
// })

// Create method to compare password

module.exports = mongoose.model('Admin', AdminSchema);
