import mongoose from 'mongoose';

const PlayerSchema = new mongoose.Schema({
  name: {
    type: Object,
    required: true,
    lowercase: true,
    first_name: {
      type: String,
      lowercase: true,
      required: true,
    },
    last_name: {
      type: String,
      lowercase: true,
      required: true,
    },
  },
  role: {
    type: String,
    lowercase: true,
    default: 'client',
  },
}, { runSettersOnQuery: true });

module.exports = mongoose.model('Player', PlayerSchema);
