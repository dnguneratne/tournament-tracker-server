import config from './config/config';
import passport from './config/passport'; // eslint-disable-line no-unused-vars
import mongoose from './config/mongoose'; // eslint-disable-line no-unused-vars
import app from './config/express';
import logger from './config/winston';

const port = config.port || '4000';
app.set('port', port);

app.listen(app.get('port'), () => {
  logger.log('info', `Server started on port ${app.get('port')} (${config.env})`);
});

export default app;
