import express from 'express';
import validate from 'express-validation';
import playerValidator from '../validators/player.validator';
import playerCtrl from '../controllers/player.controller';
import authenticate from '../modules/authentication.module';
import passport from '../config/passport';

// api/player/

const player = express.Router();

player.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  authenticate.andRestrictTo('admin'),
  playerCtrl.list,
);

player.post(
  '/register',
  validate(playerValidator.validatePlayer),
  playerCtrl.create,
);

player.param('id', playerCtrl.load);

player.put(
  '/:id',
  validate(playerValidator.validatePlayer),
  passport.authenticate('jwt', { session: false }),
  authenticate.andRestrictSelfandTo('admin'),
  playerCtrl.update,
);

player.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  authenticate.andRestrictSelfandTo('admin'),
  playerCtrl.remove,
);

export default player;
