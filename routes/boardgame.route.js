import express from 'express';
import validate from 'express-validation';
import boardgameValidator from '../validators/boardgame.validator';
import boardgameCtrl from '../controllers/boardgame.controller';
import passport from '../config/passport';
import authenticate from '../modules/authentication.module';

// api/boardgame

const boardgame = express.Router();

boardgame.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  authenticate.andRestrictTo('admin'),
  boardgameCtrl.getAll,
);

boardgame.param('gameId', boardgameCtrl.load);

boardgame.get(
  '/:gameId',
  passport.authenticate('jwt', { session: false }),
  authenticate.andRestrictTo('admin'),
  boardgameCtrl.getOne,
);

boardgame.put(
  '/:gameId',
  validate(boardgameValidator.validateGame),
  passport.authenticate('jwt', { session: false }),
  authenticate.andRestrictTo('admin'),
  boardgameCtrl.update,
);

boardgame.post(
  '/addgames',
  validate(boardgameValidator.validateGame),
  passport.authenticate('jwt', { session: false }),
  authenticate.andRestrictTo('admin'),
  boardgameCtrl.create,
);

export default boardgame;
