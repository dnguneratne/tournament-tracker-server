import express from 'express';
import authRoutes from './auth.route';
import playerRoutes from './player.route';
import boardgameRoutes from './boardgame.route';
import tournamentRoutes from './tournament.route';

const router = express.Router();

router.get('/health-check', (req, res) => {
  res.send('OK');
});

router.use('/auth', authRoutes);

router.use('/player', playerRoutes);

router.use('/boardgame', boardgameRoutes);

router.use('/tournament', tournamentRoutes);

export default router;
