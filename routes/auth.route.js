import express from 'express';
import validate from 'express-validation';
import authValidator from '../validators/auth.validator';
import authCtrl from '../controllers/auth.controller';
import passport from '../config/passport';
import authentication from '../modules/authentication.module';

// api/auth

const auth = express.Router();

auth.post('/authenticate', validate(authValidator.login), authCtrl.login);

auth.param('id', authCtrl.load);

auth.get(
  '/:id/dashboard',
  passport.authenticate('jwt', { session: false }),
  authentication.andRestrictTo('admin'),
  authentication.andRestrictToSelf,

  (req, res) => {
    res.send(req.user.id);
  },
);

export default auth;
