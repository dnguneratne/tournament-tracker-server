import express from 'express';
import validate from 'express-validation';
// import playerValidator from '../validators/player.validator';
import tournamentCtrl from '../controllers/tournament.controller';
import authenticate from '../modules/authentication.module';
import passport from '../config/passport';

// api/tournament/

const tournament = express.Router();

tournament.get(
  '/',
  // passport.authenticate('jwt', { session: false }),
  // authenticate.andRestrictTo('admin'),
  tournamentCtrl.list,
);

tournament.param('tournamentId', tournamentCtrl.load);

tournament.get(
  '/:tournamentId',
  // passport.authenticate('jwt', { session: false }),
  // authenticate.andRestrictTo('admin'),
  tournamentCtrl.getOne,
);

tournament.post(
  '/register',
  // validate(playerValidator.register),
  // passport.authenticate('jwt', { session: false }),
  // authenticate.andRestrictTo('admin'),
  tournamentCtrl.create,
);

// tournament.param('id', tournamentCtrl.load);

// tournament.put(
//   '/:id',
//   validate(tournamentCtrl.register),
//   passport.authenticate('jwt', { session: false }),
//   authentication.andRestrictSelfandTo('admin'),
//   tournamentCtrl.update,
// );

export default tournament;
