import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import logger from '../config/winston';

function andRestrictTo(role) {
  return (req, res, next) => {
    logger.log('info', `RestrictTo Role: ${role} authentication request by User: ${req.authenticatedUser.name.first_name} ${req.authenticatedUser.name.last_name}`);
    if (req.authenticatedUser.role === role) {
      logger.log('info', `Authentication granted to User: ${req.authenticatedUser.name.first_name} ${req.authenticatedUser.name.last_name}`);
      return next();
    }
    logger.log('error', `User: ${req.authenticatedUser.name.first_name} ${req.authenticatedUser.name.last_name} Role: ${req.authenticatedUser.role}. Not ${role}`);
    return next(new APIError('Forbidden', httpStatus.FORBIDDEN, true));
  };
}

function andRestrictToSelf(req, res, next) {
  logger.log('info', `RestrictToSelf authentication request by User: ${req.authenticatedUser.name.first_name} ${req.authenticatedUser.name.last_name}`);
  if (req.authenticatedUser.id === req.user.id) {
    logger.log('info', `Authentication granted to User: ${req.user.name.first_name} ${req.user.name.last_name}`);
    return next();
  }
  logger.log('error', `User: ${req.authenticatedUser.name.first_name} ${req.authenticatedUser.name.last_name} is not the same as requested user`);
  return next(new APIError('Forbidden', httpStatus.FORBIDDEN, true));
}

function andRestrictSelfandTo(role) {
  return (req, res, next) => {
    logger.log('info', `RestrictToSelfandTo authentication request by User: ${req.authenticatedUser.name.first_name} ${req.authenticatedUser.name.last_name}`);

    if (req.authenticatedUser.role === role || req.authenticatedUser.id === req.user.id) {
      logger.log('info', `Authentication granted to User: ${req.authenticatedUser.name.first_name} ${req.authenticatedUser.name.last_name}`);
      return next();
    }
    logger.log('error', `User: ${req.authenticatedUser.name.first_name} ${req.authenticatedUser.name.last_name} Role: ${req.authenticatedUser.role}. RestrictToSelfandTo access denied`);
    return next(new APIError('Forbidden', httpStatus.FORBIDDEN, true));
  };
}

export default { andRestrictTo, andRestrictToSelf, andRestrictSelfandTo };
