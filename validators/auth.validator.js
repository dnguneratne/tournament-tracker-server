import Joi from 'joi';

export default {
  // POST /api/auth/authenticate
  login: {
    body: {
      username: Joi.string().required().lowercase(),
      password: Joi.string().required().min(5).lowercase(),
    },
  },
};
