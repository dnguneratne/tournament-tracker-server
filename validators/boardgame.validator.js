import Joi from 'joi';

export default {
  // POST /api/boardgame/
  validateGame: {
    body: {
      name: Joi.string().required().lowercase(),
      scoring: Joi.object().required().keys({
        rank: Joi.object().required().keys({
          first_place: Joi.number().required().min(0).max(100),
          second_place: Joi.number().required().min(0).max(100),
          third_place: Joi.number().required().min(0).max(100),
        }),
      }),
      max_num_players: Joi.number().required(),
      min_num_players: Joi.number().required(),
    },
  },
};
