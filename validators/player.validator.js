import Joi from 'joi';

export default {
  // POST /api/player
  validatePlayer: {
    body: {
      name: Joi.object().required().keys({
        first_name: Joi.string().required().lowercase(),
        last_name: Joi.string().required().lowercase(),
      }),
    },
  },
};
